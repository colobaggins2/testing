/**
* @summary MUCBA / Buenos Aires / Argentina.
*
* @description - then i will add this.
*
* @author Aditivo Interactive Group S.A.
*
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}
*
* @todo Complete documentation.
*/

//usemos el modo estricto siempre que laburemos en JS > https://www.w3schools.com/js/js_strict.asp

"use strict";


/**
* @function Project
* @description Initialize project...
*/

var artists;

var Project = function () {

    //No modificar estas variables. Son parte del core de Aditivo

    var config = new ProjectConfig();

    var languagesManager;

    var projectUtils = new ProjectUtils();

    var helper = new Helpers();

    var iddle = new IddleManager();

    var iddleTimeOut;

    var isInMain = true;

    var currentSection;


    //zoom

    var zoomedImgSrc;

    var zoomedImg;
    
    var zoomedImgHeight;
        
    var zoomScale;


    var is_stageBlocked = false;


    /**
    * @function addListeners
    * @description ...
    *
    */

    var addListeners = function () {

        $(".iddle").on("touchstart", activeApp);

        $(".page").on("click", ".btn-expand", openPhotoGallery);

        $(".menu").on("touchstart", ".menu__item", selectSection);

        $("#gallery").on("touchstart", ".btn-expand-info", expandInfo);


        $(document).on("touchstart", ".zoom-btn", newZoom);

        $(document).on("touchstart", ".zoom-out-btn", outZoom);



        $(".gallery-container").on("touchstart", closeGallery);

        $(".close-gallery").on("touchstart", closeGallery);

        $(".grid-wrap").on("touchstart", ".gridback", goMain);



        /*
        *   No borrar - Parte del core de Aditivo
        */
        $(document).keypress(function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);

            switch (keycode) {

                case 109: // M
                    config.toggleMouse();
                    break;
                case 110: // N
                    config.toggleContextMenu();
                    break;
                default:
                    break;

            }

        });


        //No borrar. Eventos custom de Aditivo.

        window.addEventListener("mousedown", resetTimer);

        window.addEventListener("touchstart", resetTimer);

        document.addEventListener("onLoad", onDataLoaded);

        document.addEventListener("onLoadByLanguage", parseLanguageData);

        document.addEventListener('onEnter', onEnterIddleMode);

        document.addEventListener("onIddleMode", startIddleMode);

        document.addEventListener("offIddleMode", stopIddleMode);


        /*
        document.addEventListener('touchstart', function(e) {
            // Cache the client X/Y coordinates
            clientX = parseInt(e.touches[0].clientX);
            clientY = parseInt(e.touches[0].clientY);


        }, false);
        */

    }



    /**
    * @function loadConfigData
    * @description Carga los json correspondientes el config y despues la data segun el lenguaje
    *
    */

    var loadConfigData = function () {

        config.getData();

    }


    /**
    * @function onDataLoaded
    * @description Callback cuando se confirma la carga de data del config.json
    *
    */

    var onDataLoaded = function (e) {


        config.setDefaults();

        var lang;

        if (config.rememberLanguage == true) {

            lang = localStorage.getItem('language');

            if (lang == null || lang == undefined) {
                lang = config.defaultLanguage;
            }

        } else {
            lang = config.defaultLanguage;
        }


        languagesManager = new LanguagesManager(config.defaultLanguage);

        languagesManager.loadDataByLanguage(config.defaultLanguage, false);

    }


    /**
    * @function onEnterIddleMode
    * @description Callback de cuando se confirma la carga del json por lenguaje
    *
    */
    var onEnterIddleMode = function (e) {

        console.log("onEnterIddleMode callback");

    }



    /**
    * @function parseLanguageData
    * @description Callback de cuando se confirma la carga del json por lenguaje
    *
    */

    var parseLanguageData = function (e) {

        var content = e.detail.content;

        entryPoint();

    }


    /**
    * @function entryPoint
    * @description Punto de entrada. Aca se asigna el contenido de json a las variables a utilizar.
    *              Desde aca arrancas vos
    *
    */

    var entryPoint = () => {

        parseJson();

    }


    /**
    * @function parseJsonExample
    * @description example
    *
    */
    var parseJson = () => {

        sections = languagesManager.currentLanguageData.sections

        loadSections(sections);

        iddle.setIddleMode(config.iddleTimeCheck);

    }





    /**
    * @function loadSections
    * @description ...
    *
    */

    var loadSections = (sections) => {

        var bgGallery = "";
        var menuItem1 = "";
        var menuItem2 = "";
        var gallery = "";

        $.each(sections, function (i, v) {

            var currentId = parseInt(i) + 1;

            bgGallery += '<div class="bg-gallery bg-gallery--' + currentId + '">';

            $.each(v.buildings, function (j, k) {
                bgGallery += '<img class="bg-gallery__item" src="images/assets/' + k.jpg + '" alt="' + v.title + '" style="grid-area: ' + k.gridArea + '">';
            });

            bgGallery += '</div>';

            if (v.column == 1) {

                if (i == 0) {
                    menuItem1 += '<a class="menu__item menu__item--selected" data-section="' + i + '" href="#content-' + currentId + '">';
                } else {
                    menuItem1 += '<a class="menu__item" data-section="' + i + '" href="#content-' + currentId + '">';
                }

                menuItem1 += '<span class="menu__item-title">' + v.title + '</span>';
                menuItem1 += '<span class="menu__item-deco">|</span>';
                menuItem1 += '<span class="menu__item-cta"><span>Explorar</span></span>';
                menuItem1 += '</a>';
            } else {

                if (i == 0) {
                    menuItem2 += '<a class="menu__item menu__item--selected" data-section="' + i + '" href="#content-' + currentId + '">';
                } else {
                    menuItem2 += '<a class="menu__item" data-section="' + i + '" href="#content-' + currentId + '">';
                }

                menuItem2 += '<span class="menu__item-title">' + v.title + '</span>';
                menuItem2 += '<span class="menu__item-deco">|</span>';
                menuItem2 += '<span class="menu__item-cta"><span>Explorar</span></span>';
                menuItem2 += '</a>';
            }


            gallery += '<div class="content" id="content-' + currentId + '" data-bgcolor="#da9b5b">';
            gallery += '<a class="content__back" href="#menu" aria-label="Volver al menú">';
            gallery += '<svg class="icon icon--arrow"><use xlink:href="#icon-arrow"></use></svg>';
            gallery += '</a>';
            gallery += '<h2 class="content__title"><span>' + v.title + '</span></h2>';
            gallery += '<p class="content__intro"><span>' + v.subtitle + '</span></p>';
            gallery += '<div class="gallery">';
            $.each(v.buildings, function (j, k) {
                gallery += '<figure class="gallery__figure">';
                gallery += '<img class="gallery__figure-img btn-expand" data-slide="' + j + '" src="images/assets/' + k.jpg + '" alt="' + v.title + '">';
                //gallery += '<figcaption class="gallery__figure-caption">' + k.epigraph + '</figcaption>';
                gallery += '</figure>';
            });
            gallery += '</div>';
            gallery += '</div>';

        });

        $(".bg-gallery-wrap").html(bgGallery);
        $(".column-1").html(menuItem1);
        $(".column-2").html(menuItem2);
        $(".page").html(gallery);


    }




    var selectSection = function () {

        var selectedSection = $(this).attr("data-section");

        currentSection = selectedSection;

    }




    /**
    * @function openPhotoGallery
    * @description open the bootstrap carousel with the section's photos
    *
    */


    var openPhotoGallery = function () {

        blockStage();

        var currentSlide = $(this).attr("data-slide");

        var strGalleryElmt = "";
        var strDotsElmt = "";

        console.log("currentSection");
        console.log(`currentSlide ${currentSlide}`);

        $.each(sections, function (i, v) {

            
            console.log(`i: ${i}`);

            if (i == currentSection) {

                $.each(v.buildings, function (j, k) {

                    console.log(k);

                    if (k.jpg != null) {

                        if (j == 0) {

                            strGalleryElmt += '<div class="carousel-item active">';
                            strDotsElmt += '<li data-target="#gallery" data-slide-to="' + j + '" class="active"></li>';

                        } else {

                            strGalleryElmt += '<div class="carousel-item">';
                            strDotsElmt += '<li data-target="#gallery" data-slide-to="' + j + '"></li>';

                        }

                        strGalleryElmt += '<div class="image-container">';


                        strGalleryElmt += '<a class="zoom-btn"><i class="fas fa-search-plus"></i></a>';


                        strGalleryElmt += '<img src="images/assets/' + k.jpg + '" class="d-block img-fluid image-building" alt="' + v.title + '">';




                        //strGalleryElmt += '<a class="zoom-btn"><i class="fas fa-search-plus"></i></a>';
                       
                       
                       
                       
                        strGalleryElmt += '<div class="carousel-caption d-none d-block">';
                        strGalleryElmt += '<p class="info-trucated">' + truncate(k.info, 95) + '</p>';
                        strGalleryElmt += '<p class="info-full d-none">' + k.info + '</p>';
                        //strGalleryElmt += '<p>' + k.info + '</p>';
                        strGalleryElmt += '</div>';
                        strGalleryElmt += '<span class="epigraph">' + k.epigraph + '</span>';
                        strGalleryElmt += '</div>';
                        strGalleryElmt += '</div>';
                    }

                });

            }

        });


        $("#gallery .carousel-inner").html(strGalleryElmt);
        $("#gallery .carousel-indicators").html(strDotsElmt);

        setTimeout(() => {
            TweenMax.to($(".gallery-container"), .5, {
                opacity: "1", ease: Power3.easeOut, onStart: () => {

                    $('.carousel').carousel(parseInt(currentSlide));
                    $(".gallery-container").css("display", "block");

                },

                onComplete: ()=>{

                    console.log(`desbloquear...`);

                    unBlockStage();

                }

            });
        }, 750);



    }





    /**
    * @function truncate
    * @description ...
    *
    */
    var truncate = function (str, n) {
        return (str.length > n) ? str.substr(0, n - 1) + '&hellip; <a class="btn-expand-info">Ver más</a>' : str;
    };


    /**
    * @function expandInfo
    * @description ...
    *
    */
    var expandInfo = function () {

        var cc = $(this).closest(".carousel-caption");


        if (cc.find(".info-full").hasClass("d-none")) {
            cc.find(".info-full").removeClass("d-none");
        }

        if (!cc.find(".info-trucated").hasClass("d-none")) {
            cc.find(".info-trucated").addClass("d-none");
        }
    }




    var newZoom = function()
    {

        blockStage();

        zoomedImgSrc = $(this).parent().find('img').attr('src')

        zoomedImg = $('.img-overlay').find('.theimg');

        $('.img-overlay').find('.theimg').attr('src', zoomedImgSrc);

    
        zoomedImgHeight = $('.img-overlay').find('.theimg').height();
        

        zoomScale = (zoomedImgHeight <= 500) ? 1.6 : 1.3;


        TweenMax.to(zoomedImg, .5, {

            opacity: "1", scale:zoomScale, y:'15vh', ease: Power3.easeOut, 
            
            onStart: () => {
                

                $('.img-overlay').css('display', 'block');

            },
            
            onComplete: () => {
    
                $('.carousel-control-prev').css('opacity', "0");
                $('.carousel-control-next').css('opacity', "0");
                $('.gallery-container').find('.close-gallery').css('opacity', "0");

                unBlockStage();

            }

        });

        

    }

    var outZoom = function()
    {

        blockStage();
        
        console.log(`cerrar zoom`);


        TweenMax.to($('.img-overlay').find('.theimg'), .5, {

            opacity: "0", scale:1, ease: Power3.easeOut, 
            
            onStart: () => {

                $('.carousel-control-prev').css('opacity', "1");
                $('.carousel-control-next').css('opacity', "1");
                $('.gallery-container').find('.close-gallery').css('opacity', "1");

            },
            
            onComplete: () => {
                
                $('.img-overlay').css('display', 'none');

                unBlockStage();

            }

        });

    }


    /**
    * @function zoomImage
    * @description ...
    *
    */
     var zoomImage = function () {

        /*
        if (!isZooming) {

            isZooming = true;

            new Zooming({
                bgColor: 'rgba(0, 0, 0, 0)',
            }).listen('.image-building')

        }
        */

        /*
        if (!$(".carousel-caption").hasClass("inactive")) {
            $(".carousel-caption").addClass("inactive")
        }

        if (!$(".epigraph").hasClass("inactive")) {
            $(".epigraph").addClass("inactive")
        }
        */

    }





    /**
    * @function cancelZoom
    * @description ...
    *
    */
    var cancelZoom = function () {

       /* if ($(".carousel-caption").hasClass("inactive")) {
            $(".carousel-caption").removeClass("inactive")
        }

        if ($(".epigraph").hasClass("inactive")) {
            $(".epigraph").removeClass("inactive")
        }*/
    }






    /**
    * @function closeGallery
    * @description close the bootstrap carousel
    *
    */
    var closeGallery = function (e) {

        console.log(`close gallery`);


        if (e.target !== this)
            return;

        
        blockStage();

        TweenMax.to($(".gallery-container"), .5, {
            opacity: "0", ease: Power3.easeOut, onComplete: () => {

                $(".gallery-container").css("display", "none");

                unBlockStage();

            }
        });
    }





    /**
    * @function goMain
    * @description Add d-none to section's subtitle
    *
    */


    var goMain = function () {


        $(".grid__section-info").each(function () {

            if (!$(this).hasClass("d-none")) {
                $(this).addClass("d-none")
            }

        });
    }



    var returnToScreenSaver = function () {

        TweenMax.to($(".iddle"), .5, { opacity: "1", ease: Power3.easeOut, onStart: () => { $(".iddle").css("display", "block"); isInMain = true; } });
        location.reload();
    }




    // IDDLE
    /**********************************************
    *
    *
    */




    /**
    * @function initApp
    * @description start app when click on iddle
    *
    */



    var activeApp = function () {

        iddle.returnFromIddle();

    }


    /**
    * @function setIddleMode
    * @description callback de que la pantalla esta activa
    *
    */

    var startIddleMode = function () {

        if (!iddle.isIddle) {

            iddleTimeOut = setTimeout(() => {

                iddle.isIddle = true;

                if (!isInMain) {

                    returnToScreenSaver();
                }

            }, iddle.timeToIddle);
        }
    }



    var resetTimer = function () {

        window.clearTimeout(iddleTimeOut);

        startIddleMode();

    }



    /**
    * @function setIddleMode
    * @description callback de que la pantalla esta activa
    *
    */

    var stopIddleMode = function () {

        if (iddle.isIddle) {

            iddle.isIddle = false;

            TweenMax.to($(".iddle"), .5, { opacity: "0", ease: Power3.easeOut, onComplete: () => { $(".iddle").css("display", "none"); isInMain = false; } });

        }
    }


    /**
	* @function blockStage
	* @description
	*/
	var blockStage = function(){

		console.log(`blockUi... from main.js`);


        if(!is_stageBlocked){

            //$.blockUI({ message: null, overlayCSS: { backgroundColor: '#00f' } });
            $.blockUI({ message: null, overlayCSS: { backgroundColor: '#00f', opacity:'0' } });

            is_stageBlocked = true;

        }

	}

	/**
	* @function unBlockStage
	* @description
	*/
	var unBlockStage = function(){

		console.log(`unBlockUi...from main.js`);

        if(is_stageBlocked){

            $.unblockUI();

            is_stageBlocked = false;

        }

	}


    return {

        init: function () {

            loadConfigData();

            addListeners();

        }

    }

}();

$(document).ready(function () { Project.init(); });
